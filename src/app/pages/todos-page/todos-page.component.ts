import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { TodosService } from './todos.service';

@Component({
  selector: 'app-todos-page',
  templateUrl: './todos-page.component.html',
  styleUrls: ['./todos-page.component.scss'],
  providers: [TodosService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodosPageComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<void>();

  constructor(public todos_service: TodosService, private router: Router) {}

  public ngOnInit(): void {
    this.todos_service.current_todo_id$.pipe(takeUntil(this.destroyed$)).subscribe((id) => this.router.navigate([id]));
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
  }

  public addNewtodo() {
    this.todos_service.todos$.pipe(first()).subscribe((todos) => {
      const last_todo = todos[todos.length - 1];
      todos.push({
        id: String(Number(last_todo.id) + 1),
        title: 'Новая заметка',
        text: 'Текст заметки'
      });
      this.todos_service.todos$.next([...todos]);
    });
  }
}
